cd %~dp0\..\..

if %1==wasm goto WASM
if %1==doc goto DOC
if %1==package goto PACKAGE
if %1==test goto TEST
if %1==publish goto PUBLISH
if %1==image goto IMAGE
if %1==deploy goto DEPLOY

:WASM
docker run --rm  ^
--env CI_COMMIT_SHORT_SHA=123456 ^
--env CI_SERVER_HOST="%GITLAB_CI_SERVER_HOST%" ^
--env CI_PROJECT_ID="%GITLAB_PROJECT_ID_WPDEMO%" ^
--env PERSONAL_ACCESS_TOKEN="%GITLAB_PERSONAL_ACCESS_TOKEN%" ^
--env GITLAB_INSTANCE="%GITLAB_INSTANCE%" ^
-v %cd%:/code -w /code %IMAGE_REGISTRY_PATH_MAIN%/emscripten/emsdk:2.0.34 bash -c "cd /code && source .scripts/cicd/jobs.sh; build-wasm"
goto:eof

:PACKAGE
rmdir /s /q package
docker run --rm  ^
--env CI_COMMIT_SHORT_SHA=123456 ^
--env CI_SERVER_HOST="%GITLAB_CI_SERVER_HOST%" ^
--env CI_PROJECT_ID="%GITLAB_PROJECT_ID_WPDEMO%" ^
--env PERSONAL_ACCESS_TOKEN="%GITLAB_PERSONAL_ACCESS_TOKEN%" ^
--env GITLAB_INSTANCE="%GITLAB_INSTANCE%" ^
--env NODE_HEADERS_URL=%NODE_HEADERS_URL% ^
-v %cd%:/code -w /code %IMAGE_REGISTRY_PATH_MAIN%/node:18.17.1 bash -c "cd /code && source .scripts/cicd/jobs.sh; build-package"
goto:eof

:TEST
docker run --rm  ^
--env CI_COMMIT_SHORT_SHA=123456 ^
--env CI_SERVER_HOST="%GITLAB_CI_SERVER_HOST%" ^
--env CI_PROJECT_ID="%GITLAB_PROJECT_ID_WPDEMO%" ^
--env PERSONAL_ACCESS_TOKEN="%GITLAB_PERSONAL_ACCESS_TOKEN%" ^
--env GITLAB_INSTANCE="%GITLAB_INSTANCE%" ^
-v %cd%:/code -w /code %IMAGE_REGISTRY_PATH_MAIN%/node:18.17.1 bash -c "cd /code && source .scripts/cicd/jobs.sh; test-package"
goto:eof

:PUBLISH
docker run --rm  ^
--env CI_COMMIT_SHORT_SHA=123456 ^
--env CI_SERVER_HOST="%GITLAB_CI_SERVER_HOST%" ^
--env CI_PROJECT_ID="56114016" ^
--env PERSONAL_ACCESS_TOKEN="%GITLAB_PERSONAL_ACCESS_TOKEN%" ^
--env GITLAB_INSTANCE="%GITLAB_INSTANCE%" ^
-v %cd%:/code -w /code %IMAGE_REGISTRY_PATH_MAIN%/node:18.17.1 bash -c "cd /code && source .scripts/cicd/jobs.sh; publish-package"
goto:eof

:IMAGE
docker exec ^
--env CI_COMMIT_SHORT_SHA=123456 ^
--env CI_SERVER_HOST="%GITLAB_CI_SERVER_HOST%" ^
--env CI_PROJECT_ID="%GITLAB_PROJECT_ID_WPDEMO%" ^
--env CI_REGISTRY=%GITLAB_CI_REGISTRY% ^
--env CI_REGISTRY_IMAGE=%GITLAB_CI_REGISTRY%/dspdf/pandemic ^
--env PERSONAL_ACCESS_TOKEN="%GITLAB_PERSONAL_ACCESS_TOKEN%" ^
--env GITLAB_INSTANCE="%GITLAB_INSTANCE%" ^
--env IMAGE_REGISTRY_PATH=%IMAGE_REGISTRY_PATH_MAIN% ^
--env NODE_HEADERS_URL=%NODE_HEADERS_URL% ^
-it docker-dind /bin/sh -c "cd /code && source .scripts/cicd/dind_image.sh"
goto:eof

:DEPLOY
docker run --rm  ^
--env CI_COMMIT_SHORT_SHA=123456 ^
--env CI_SERVER_HOST="%GITLAB_CI_SERVER_HOST%" ^
--env CI_PROJECT_ID="%GITLAB_PROJECT_ID_WPDEMO%" ^
--env CI_REGISTRY=%GITLAB_CI_REGISTRY% ^
--env CI_REGISTRY_IMAGE=%GITLAB_CI_REGISTRY%/dspdf/pandemic ^
--env PERSONAL_ACCESS_TOKEN="%GITLAB_PERSONAL_ACCESS_TOKEN%" ^
--env GITLAB_INSTANCE="%GITLAB_INSTANCE%" ^
--env IMAGE_REGISTRY_PATH=%IMAGE_REGISTRY_PATH_MAIN% ^
--env SSHKEY_APP_SERVER="id_rsa.appServer" ^
-v %cd%:/code -w /code %IMAGE_REGISTRY_PATH%/dspdf-rocky8 bash -c "cd /code && source .scripts/cicd/jobs.sh; deploy-image"
goto:eof

:DOC
docker exec ^
--env CI_COMMIT_SHORT_SHA=123456 ^
--env CI_SERVER_HOST="%GITLAB_CI_SERVER_HOST%" ^
--env CI_PROJECT_ID="%GITLAB_PROJECT_ID_WPDEMO%" ^
--env PERSONAL_ACCESS_TOKEN="%GITLAB_PERSONAL_ACCESS_TOKEN%" ^
--env GITLAB_INSTANCE="%GITLAB_INSTANCE%" ^
--env IMAGE_REGISTRY_PATH=%IMAGE_REGISTRY_PATH_MAIN% ^
--env MATHJAX_URL=%MATHJAX_URL% ^
-it docker-dind /bin/sh -c "cd /code && source .scripts/cicd/dind_doc.sh"
goto:eof


