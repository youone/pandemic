FROM emscripten/emsdk:2.0.34

WORKDIR /wasm

RUN apt-get update &&\
curl -sL https://deb.nodesource.com/setup_18.x | bash - &&\
apt-get -y install \
libeigen3-dev \
gcc-multilib \
rapidjson-dev \
openssh-server \
libgeographic-dev \
libeigen3-dev \
libfftw3-dev \
gdb \
cmake \
nlohmann-json3-dev \
jq &&\
wget http://www.fftw.org/fftw-3.3.10.tar.gz &&\
wget https://sourceforge.net/projects/geographiclib/files/distrib/GeographicLib-1.52.tar.gz &&\
tar xvzf fftw-3.3.10.tar.gz &&\
tar xvzf GeographicLib-1.52.tar.gz &&\
cd /wasm/fftw-3.3.10 &&\
emconfigure ./configure && emmake make &&\
mv /usr/lib/x86_64-linux-gnu/libfftw3.a /usr/lib/x86_64-linux-gnu/libfftw3.a.c &&\
cp -v .libs/libfftw3.a /usr/lib/x86_64-linux-gnu &&\
mv api /usr/local/include/fftw3 &&\
cd /wasm/GeographicLib-1.52 &&\
emconfigure ./configure && emmake make &&\
mv /usr/lib/x86_64-linux-gnu/libGeographic.a /usr/lib/x86_64-linux-gnu/libGeographic.a.c &&\
cp -v src/.libs/libGeographic.a /usr/lib/x86_64-linux-gnu/ &&\
mv include/GeographicLib /usr/local/include &&\
rm -rf /wasm/fftw-3.3.10* &&\
rm -rf /wasm/GeographicLib-1.52*

# RUN mkdir /var/run/sshd
# RUN ssh-keygen -A
# RUN echo 'root:password' | chpasswd
# RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
# RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
# EXPOSE 22

# CMD ["/usr/sbin/sshd", "-D", "-o", "ListenAddress=0.0.0.0"]
