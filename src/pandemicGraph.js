import * as d3 from 'd3';
import interact from "interactjs";

export class PandemicPlot extends HTMLElement {

    constructor(height, type) {
        super()
        this.nBins = 200;
        this.plotHeight = height;
        this.type = type;
        // this.ymax = ymax;
        this.models = {};
        this.margin = {top: 20, right: 30, bottom: 30, left: 75};

    }

    addModel(name, model) {
        model.name = name;
        this.models[name] = {model: model};
        this.models[name].tBrushes = {};
        this.models[name].rBrushes = {};
        this.models[name].path = null;
        this.models[name].selected = false;

        model.onSelect((m) => {
            this.selectModel(m.name);
        });
    
        // model.onChange((parameters) => {
        //     console.log('MODEL CHANGED', parameters);
        // })
    }

    update() {
        Object.values(this.models).forEach(m => {
            m.model.update();
        });        
    }

    connectedCallback() {
        const template = `
            <style>
                .rcontrol {
                    cursor: pointer !important;
                }
            </style>
            <div id="content"></div>
        `;

        this.shadow = this.attachShadow({mode: 'open'});
        this.shadow.innerHTML = template;
        this.content = this.shadow.querySelector('#content');

        this.width = 600 - this.margin.left - this.margin.right;
        this.height = this.plotHeight - this.margin.top - this.margin.bottom;

        //x and y scales generators
        this.xScale = d3.scaleLinear().range([0, this.width]).domain([0, this.nBins]);
        this.yScale = d3.scaleLinear().range([this.height, 0]).domain([0, this.type === 'rvalue' ? 3.5 : 150000]);

        //x and y axes generators
        this.xAxis = d3.axisBottom().scale(this.xScale);
        this.yAxis = d3.axisLeft().scale(this.yScale);

        this.svg = d3.select(this.content).append("svg")
            .attr("width", this.width + this.margin.left + this.margin.right)
            .attr("height", this.height + this.margin.top + this.margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + this.margin.left + "," + this.margin.top + ")");

        this.svg.append("g")
            .attr("class", "x-axis")
            .attr("transform", "translate(0," + this.height + ")")
            .call(this.xAxis);

        this.svg.append("g")
            .attr("class", "y-axis")
            .call(this.yAxis);

        console.log(this.xScale.invert(0));

        if (this.type !== 'rvalue') return;

    }


    // update(data) {

    //     const dta = data;

    //     document.querySelector('.datapath').remove();

    //     if (this.type === 'cases') {
    //         const maxValue = d3.max(dta.map(d => d.value));
    //         console.log(maxValue);
    //         this.yScale.domain([0, 1.5*maxValue]);
    //         this.svg.select(".y-axis")
    //         .call(this.yAxis);
    //     }


    //     this.svg.append("path")
    //         .datum(dta)
    //         .attr("class", "datapath")
    //         .attr("fill", "none")
    //         .attr("stroke", "steelblue")
    //         .attr("stroke-width", 1.5)
    //         .attr("d", d3.line()
    //             .x((d) => {
    //                 // console.log(d);
    //                 return this.xScale(d.day);
    //             })
    //             .y((d) =>  {
    //                 return this.yScale(d.value);
    //             })
    //         )
    // }

    // onParameterChange(handler) {
    //     this.parameterChangeHandler = handler;
    // }

}

export class PandemicRPlot extends PandemicPlot {

    constructor() {
        super(200, 'rvalue')
    };

    connectedCallback() {
        super.connectedCallback();
        this.svg.append("path")
        .datum([{day:0, value:1}, {day:200, value:1}])
        .attr("class", "datapath")
        .attr("fill", "none")
        .attr("stroke", "#000000")
        .attr("stroke-width", 0.5)
        .attr("stroke-dasharray", "10,5")
        .attr("d", d3.line()
            .x((d) => {
                // console.log(d);
                return this.xScale(d.day);
            })
            .y((d) =>  {
                return this.yScale(d.value);
            })
        )

    }

    addModel(name, model) {
        super.addModel(name, model);

        model.onChange((mod) => {
            console.log('MODEL CHANGED R', mod);

            Object.values(this.models).forEach(m => {
                const dta = m.model.rData;
                
                if (m.path) m.path.node().remove();

                m.path = this.svg.append("path")
                .datum(dta)
                .attr("class", "datapath")
                .attr("fill", "none")
                .attr("stroke", m.model.color)
                .attr("opacity", 0.5)
                .attr("stroke-width", m.selected ? 2 : 1)
                .attr("d", d3.line()
                    .x((d) => {
                        // console.log(d);
                        return this.xScale(d.day);
                    })
                    .y((d) =>  {
                        return this.yScale(d.value);
                    })
                )
            });

        })

        const xBrush = d3.brushX()
        .extent([[0, 0], [this.width, 30]])
        .on("start brush end", (event) => {
            const selection = event.selection;
            const [x0, x1] = selection.map(this.xScale.invert);
            if (event.sourceEvent) {
                this.selectedModel.tBrushes.tBrush.call(xBrush.move, [Math.round(x0), Math.round(x1)].map(this.xScale));
            }
            else {
                try {
                    this.selectedModel.model.setR('start', x0);
                    this.selectedModel.model.setR('stop', x1);
                }
                catch(e) {}
            }
        });

        const yBrush = d3.brushY()
        .extent([[0, 0], [30, this.height]])
        .on("start brush end", (event) => {
            const selection = event.selection;
            const [y0, y1] = selection.map(this.yScale.invert);
            const y0r = Math.round(y0*20)/20;
            const y1r = Math.round(y1*20)/20;
            if (event.sourceEvent) {
                this.selectedModel.rBrushes.rBrush.call(yBrush.move, [y0r, y1r].map(this.yScale));
            }
            else {
                try {
                    this.selectedModel.model.setR('min', y1r);
                    this.selectedModel.model.setR('max', y0r);
                }
                catch(e) {}
            }
        });

        const tBrush = this.svg.append("g")
        .attr("transform", "translate(0,-25)")
        .call(xBrush)
        .call(xBrush.move, [model.start, model.start + model.width].map(this.xScale))
        // .call(g => g.select(".overlay")
        //     .datum({type: "selection"})
        //     .on("mousedown touchstart", () => {}));

        const rBrush = this.svg.append("g")
        .attr("transform", "translate(505,0)")
        .call(yBrush)
        .call(yBrush.move, [model.Rstart, model.Rend].map(this.yScale))

        this.models[name].tBrushes.tBrush = tBrush;
        this.models[name].rBrushes.rBrush = rBrush;

        this.selectModel(name);
    }

    selectModel(name) {
        this.selectedModel = this.models[name];
        Object.values(this.models).forEach(m => {

            m.selected = m.model.name === name ? true : false
            m.tBrushes.tBrush.node().style.display = m.model.name === name ? 'block' : 'none';
            m.rBrushes.rBrush.node().style.display = m.model.name === name ? 'block' : 'none';
            if (m.path) m.path.node().setAttribute('stroke-width', m.model.name === name ? 2 : 1);
            if (m.path) m.path.node().style['z-index'] = m.model.name === name ? 2 : 1;
        })
    }
}

export class PandemicCasePlot extends PandemicPlot {
    constructor() {
        super(300, 'cases')
    };

    addModel(name, model) {
        super.addModel(name, model);

        model.onChange((mod) => {
            console.log('MODEL CHANGED C', mod);

            console.log(mod.name);

            let maxValue = 0;
            let maxVal;

            Object.values(this.models).forEach(m => {
                const dta = m.model.nInfected;
                maxVal = d3.max(dta.map(d => d.value));
                if (maxVal > maxValue) maxValue = maxVal;
            })

            Object.values(this.models).forEach(m => {

                const dta = m.model.nInfected;

                this.yScale.domain([0, 1.5*maxValue]);

                this.svg.select(".y-axis")
                .call(this.yAxis);

                if (m.path) m.path.node().remove();

                m.path = this.svg.append("path")
                    .datum(dta)
                    .attr("class", `datapath ${mod.name}`)
                    .attr("fill", "none")
                    .attr("stroke", m.model.color)
                    .attr("opacity", 0.5)
                    .attr("stroke-width", m.selected ? 2 : 1)
                    .attr("d", d3.line()
                        .x((d) => {
                            // console.log(d);
                            return this.xScale(d.day);
                        })
                        .y((d) =>  {
                            // console.log(d);
                            return this.yScale(d.value);
                        })
                    )
            })
        })

        this.selectModel(name);

    }

    selectModel(name) {
        this.selectedModel = this.models[name];
        Object.values(this.models).forEach(m => {
            m.selected = m.model.name === name ? true : false
            if (m.path) m.path.node().setAttribute('stroke-width', m.model.name === name ? 2 : 1);
        })
    }


}

window.customElements.define('pandemic-plot', PandemicPlot);
window.customElements.define('pandemic-rplot', PandemicRPlot);
window.customElements.define('pandemic-caseplot', PandemicCasePlot);


